const Sequelize = require('sequelize')
const sequelize = new Sequelize('sqlite::memory:')

sequelize.authenticate()
  .then(() => {
    console.log('Connection has been established successfully.')
  })
  .catch(error => {
    console.error('Unable to connect to the database:', error)
  })

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.user = require('../models/User')(sequelize, Sequelize)

module.exports = db
