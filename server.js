const express = require('express')

const userRouter = require('./routes/user')
const db = require('./config/db')

const app = express()

app.use(express.json())

app.get('/', (req, res) => res.send('home'))
app.use('/users', userRouter)

db.sequelize.sync()

const PORT = process.env.PORT || 3000
app.listen(PORT, () => { console.log(`Server runing on port: ${PORT}`) })
