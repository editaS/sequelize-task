const db = require('../config/db')
const User = db.user

const getUser = async (req, res) => {
  const { userId: id } = req.params
  try {
    const user = await User.findByPk(id)
    res.status(200).json(user)
  } catch (error) {
    res.status(500).json({ message: `Couldn't get user with ID: ${id}` })
  }
}

const createUser = async (req, res) => {
  const user = req.body

  try {
    await User.create(user)
    res.send('user created')
  } catch (error) {
    res.status(500).json({ message: 'Failed to create User' })
  }
}

const addPictureUrl = async (req, res) => {
  const { userId: id } = req.params
  console.log(req.body)
  try {
    await User.update(req.body,
      {
        where: { id: id }
      }
    )
    res.status(200).json('User updated')
  } catch (error) {
    res.status(500).json({ message: `Couldn't update picture url for user with ID: ${id}` })
  }
}

module.exports = {
  getUser,
  createUser,
  addPictureUrl
}
