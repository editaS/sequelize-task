module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define('user', {
    first_name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    last_name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    birthday: {
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    bio: {
      type: Sequelize.STRING,
      allowNull: true
    },
    profile_picture_url: {
      type: Sequelize.STRING,
      allowNull: false
    },
    cover_picture_url: {
      type: Sequelize.STRING,
      allowNull: false
    },
    roleRevokedOn: {
      type: Sequelize.DATE,
      allowNull: true
    },
    roleAssigneddOn: {
      type: Sequelize.DATEONLY,
      allowNull: true
    },
    roleFevokedOn: {
      type: Sequelize.STRING,
      allowNull: true
    },
    isBanned: {
      type: Sequelize.BOOLEAN,
      allowNull: false
    },
    banReason: {
      type: Sequelize.STRING,
      allowNull: true
    }
  })

  return User
}
