const { Router } = require('express')
const { getUser, createUser, addPictureUrl } = require('../controller/User')

const router = Router()

router.post('/', createUser)
router.get('/:userId', getUser)
router.patch('/:userId', addPictureUrl)

module.exports = router
